import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class CompiledApkUpdate
{
	
	public static final String PREFIX = "Secret_2.1_";
	 private static final String androidSDK_PATH = "D:\\Android\\android_sdk";        //android SDK璺緞

	    public static final String APK_NAME = "Secret_final_1.3_old.apk";
	    public static final String PROJECT_LIBARY = "";
	    public static final String PROJECT_PATH = "D:\\repos\\secret-android\\source\\";        //瑕佹墦鍖呯殑宸ョ▼璺緞
	    public static final String APK_PATH = "D:\\"; 
	    
	    private static final String apk_PATH_keystore = "D:\\repos\\secret-android\\secret_key";        //apk绛惧悕鏂囦欢璺緞
	    private static final String channelFlag = "app_channel";
	    
	    
	    private static String currentChannelName = "";
	    public static String[] channels = {
//	    	"google_play",
//	    	"an_zhuo",
//	    	"ying_yong_hui",
////	    	"n_duo",
////	    	"ying_yong_bao",
////	    	"jifeng",
//	    	"update",
	    	"360"
////	    	"web",
//	    	"baidu",
//	    	"wan_dou_jia"
	    };

	    public static void main(String[] args) { 
	        replaceChannel();
	    }

	    /**
	     * 鏇挎崲娓犻亾鍚嶇О
	     */
	    public static void replaceChannel() {
	        try {
	            String outPath = PROJECT_PATH + "res\\values\\strings.xml"; // 杈撳嚭鏂囦欢浣嶇疆
	            String content = read(outPath);
	            for(int channelid=0;channelid<channels.length;channelid++){
	                String tmpContent = content;
	                tmpContent = tmpContent.replaceFirst(channelFlag, channels[channelid]);
	                currentChannelName = channels[channelid];
	                write(tmpContent,outPath);
	                System.out.println("replace channel name over...");
	                packageRes(); // 涓�娓犻亾鍙风殑鏇存敼瀹屾垚銆傚彲浠ヨ繘琛屾墦鍖呬簡銆�
	                
	                tmpContent = tmpContent.replaceFirst(channels[channelid], channelFlag);
	                
	                write(content,outPath);        //瀹屾垚鍚庤繕鍘焎hannel_name
	                
	                Copy(PROJECT_PATH + "bin\\SplashScreen-release.apk", APK_PATH + PREFIX + currentChannelName + ".apk");
	            }
	           
	            System.out.println("execute over!");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    
	public static void Copy(String oldPath, String newPath)
	{
		try
		{
			int bytesum = 0;
			int byteread = 0;
			File oldfile = new File(oldPath);
			if (oldfile.exists())
			{
				InputStream inStream = new FileInputStream(oldPath);
				FileOutputStream fs = new FileOutputStream(newPath);
				byte[] buffer = new byte[1444];
				int length;
				while ((byteread = inStream.read(buffer)) != -1)
				{
					bytesum += byteread;
					fs.write(buffer, 0, byteread);
				}
				inStream.close();
			}
		}
		catch (Exception e)
		{
			System.out.println("error  ");
			e.printStackTrace();
		}
	}

		/**
	     * class鏂囦欢鎵撳寘鎴恈lasses.dex
	     */
	    public static void packageDex(){
	        try { 
	            System.out.println("dx.bat start...");
	            Process process = Runtime.getRuntime().exec(androidSDK_PATH
	                    +"platform-tools/dx.bat --dex --output="
	                    +PROJECT_PATH+"bin/classes.dex "
	                    +PROJECT_PATH+"bin/classes "
	                    +PROJECT_PATH+"libs/*.jar"); 
	            
	            new MyThread(process.getErrorStream()).start();

	            new MyThread(process.getInputStream()).start();
	            
	            process.waitFor();  
	            process.destroy();  
	            System.out.println("dx.bat over...");
	        } catch (Exception e) { 
	            e.printStackTrace(); 
	        } 
	    }
	    
	    /**
	     * res assets鏂囦欢鎵撳寘鎴恟es.zip
	     */
	    public static void packageRes(){
	        try{
	            System.out.println(currentChannelName+" create resources.ap");
	            String library = "";
	            if(PROJECT_LIBARY!=null&&!PROJECT_LIBARY.equals("")){
	                library = "-S " + PROJECT_LIBARY + "res ";
	            }
	            Process process = null;
	            
	            String exeStr = "D:\\apache-ant-1.9.1\\bin\\ant.bat -buildfile " + PROJECT_PATH + "build.xml clean";
//	            String exeStr = "notepad";
	            
	            process = Runtime
	                    .getRuntime()
	                    .exec(exeStr);
	            new MyThread(process.getErrorStream()).start();
	            new MyThread(process.getInputStream()).start();
	            process.waitFor();
	            process.destroy();
	            
	            exeStr = "D:\\apache-ant-1.9.1\\bin\\ant.bat -buildfile " + PROJECT_PATH + "build.xml release";
//	            exeStr = "notepad";
	            
	            process = Runtime
	                    .getRuntime()
	                    .exec(exeStr);
	            new MyThread(process.getErrorStream()).start();
	            new MyThread(process.getInputStream()).start();
	            process.waitFor();
	            process.destroy();
	            System.out.println(currentChannelName+" resources.ap over...");
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	    }
	    
	    /**
	     * classes.dex res.zip AndroidManifest.xml缁勫悎鎴愭湭绛惧悕鐨刟pk
	     */
	    public static void createUnsignedApk(){
	        try{
	            System.out.println(currentChannelName+" createUnsignedApk start");
	            Process process = null;
	            process = Runtime.getRuntime().exec(
	                    androidSDK_PATH+ "tools\\apkbuilder.bat "
	                    + PROJECT_PATH + "bin\\"+APK_NAME+" -u -z "
	                    + PROJECT_PATH + "bin\\resources.ap_ -f "
	                    + PROJECT_PATH + "bin\\classes.dex"); // 鐢熸垚鏈鍚嶇殑apk
	            new MyThread(process.getErrorStream()).start();
	            new MyThread(process.getErrorStream()).start();
	            process.waitFor();
	            process.destroy();
	            System.out.println(currentChannelName+" createUnsignedApk over");
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	    }
	    
	    /**
	     * 绛惧悕apk
	     */
	    public static void signedApk(int channelid){
	        try{
	            System.out.println(currentChannelName+" signed apk start");
	            Process process = null;
	            String jarsigner;
	            jarsigner = "jarsigner -keystore "+apk_PATH_keystore+" -storepass ***** -keypass ****** " +
	                    "-signedjar "
	                    + APK_PATH
	                    + channels[channelid]
	                    + ".apk "
	                    + PROJECT_PATH
	                    + "bin\\"+APK_NAME+" *****";            //绛惧悕apk
	            process = Runtime
	                    .getRuntime()
	                    .exec(jarsigner); // 瀵筧pk杩涜绛惧悕
	            new MyThread(process.getErrorStream()).start();

	            new MyThread(process.getInputStream()).start();
	            process.waitFor();
	            process.destroy();
	            System.out.println(currentChannelName+" signed apk over"); // 涓�潯娓犻亾鐨勬墦鍖呭畬鎴愩�鏂囦欢浼氳緭鍑哄埌鎸囧畾鐩綍
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	    }
	    
	    
	    public static String read(String path) {
	        StringBuffer res = new StringBuffer();
	        String line = null;
	        try {
	            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path),"UTF-8"));
	            while ((line = reader.readLine()) != null) {
	                res.append(line + "\n");
	            }
	            reader.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return res.toString();
	    }

	    public static boolean write(String cont, String dist) {
	        try {
	            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File(dist)),"utf-8");
	            writer.write(cont);
	            writer.flush();
	            writer.close();
	            return true;
	        } catch (IOException e) {
	            e.printStackTrace();
	            return false;
	        }
	    }
	    
	    
	    public static class MyThread extends Thread{
	        BufferedReader bf;
	        
	        public MyThread(InputStream input) {
	            bf = new BufferedReader(new InputStreamReader(input));
	        }

	        public void run() {
	            String line;
	            try {
	                line = bf.readLine();
	                while (line != null) {
	                    System.out.println(line);
	                    line = bf.readLine();
	                }
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }

}
